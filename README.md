Drosophila aka Hello World project.....my test :-) 

This is based on TF, docker, and Java.



1. Clone this code repo.

2. Configure your local with aws cli, aws credentials and terraform.

3. Use this TF scripts in your local in order to create the aws infra :
terraform init
terraform plan
terraform apply

Also this TF script will download the Java hello world application from the following repo: 
https://gitlab.com/rfdnet2021/hello-world1.git

3. Application URL 

http://public_ip:8080/hello/anything
Eg: http://ec2-3-238-37-140.compute-1.amazonaws.com:8080/hello/reinaldo

References:
https://howtodoinjava.com/java/library/docker-hello-world-example/

