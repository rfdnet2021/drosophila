terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}



provider "aws" {

  #region = var.instance_region
  region  = "us-east-1"
  profile = "default"

}