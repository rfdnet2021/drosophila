resource "aws_security_group" "drosophila_sg" {
  name   = "dev_sg"
  vpc_id = aws_vpc.drosophila_dev.id

  ingress {
    description = "Allows http inbound"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["191.193.152.227/32"] #this was my public IP !!! I need to manually update this...pls improve!
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]

  }
}