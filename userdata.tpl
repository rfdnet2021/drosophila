#!/bin/bash
sudo apt-get update -y &&
sudo apt-get install -y \
apt-transport-https \
ca-certificates \
curl \
gnupg-agent \
software-properties-common &&
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - &&
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" &&
sudo apt-get update -y &&
sudo sudo apt-get install docker-ce docker-ce-cli containerd.io -y &&
sudo usermod -aG docker ubuntu &&
git clone https://gitlab.com/rfdnet2021/hello-world1.git &&
cd hello-world1 &&
sudo docker build --progress=plain  -t hello-docker . &&
sudo docker run -p 8080:9080 -t hello-docker --name hello-docker-image

#hit my application url:
#http://ec2-ip-address:8080/hello/reinaldo
#
#sudo docker container run --name hello1 -t -d -p 8080:80 nginx:latest && sudo docker start hello1