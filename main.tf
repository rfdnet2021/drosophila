resource "aws_vpc" "drosophila_dev" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Env     = "dev"
    Project = "drosophila"
    Cost    = "0123"
  }
}

resource "aws_subnet" "drosophila_dev_public_subnet" {
  vpc_id                  = aws_vpc.drosophila_dev.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1a"

  tags = {
    Name = "dev-public"
  }
}

resource "aws_internet_gateway" "drosophila_dev_gateway" {
  vpc_id = aws_vpc.drosophila_dev.id

  tags = {
    Name = "dev-igw"
  }
}

resource "aws_route_table" "drosophila_dev_public_rt" {
  vpc_id = aws_vpc.drosophila_dev.id
  tags = {
    Name = "dev_public_rt"
  }

}

resource "aws_route" "default_route" {
  route_table_id         = aws_route_table.drosophila_dev_public_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.drosophila_dev_gateway.id

}

resource "aws_route_table_association" "drosophila_public_assoc" {
  subnet_id      = aws_subnet.drosophila_dev_public_subnet.id
  route_table_id = aws_route_table.drosophila_dev_public_rt.id

}



resource "aws_key_pair" "drosophila_auth" {
  key_name   = "drosophila_key"
  public_key = file("~/.ssh/droso-dev.pub")

}

resource "aws_instance" "dev_node" {
  instance_type          = var.instance_type
  ami                    = data.aws_ami.server_ami.id
  key_name               = aws_key_pair.drosophila_auth.id
  vpc_security_group_ids = [aws_security_group.drosophila_sg.id]
  subnet_id              = aws_subnet.drosophila_dev_public_subnet.id
  user_data              = file("userdata.tpl")

  root_block_device {
    volume_size = 10

  }
  tags = {
    Name = "dev-node"
  }

  provisioner "local-exec" {
    command = templatefile("linux-ssh-config.tpl", {
      hostname     = self.public_ip,
      user         = "ubuntu",
      identityfile = "~/.ssh/drosophila"
    })
    interpreter = ["bash", "-c"]

  }
}

resource "aws_cloudwatch_metric_alarm" "cpu-utilization" {
  alarm_name                = "cpu-utilization"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  metric_name               = "CPUUtilization"
  namespace                 = "AWS/EC2"
  period                    = "60"
  statistic                 = "Average"
  threshold                 = "10"
  alarm_description         = "This metric monitors ec2 cpu utilization"
  insufficient_data_actions = []

  dimensions = {
    InstanceId = aws_instance.dev_node.id
  }


}